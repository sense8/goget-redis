REDIS_PORT=6379
REDIS_NAME=redis-dev
REDIS_TAG=redis:6.2-rc2
DOCKER_NETWORK=bridge
REDIS_KEY=bme680.0
REDIS_DATA="{\"name\": \"climate\", \"location\": \"nyc\", \"room\": \"room\", \"sensor_name\": \"bme680\", \"sensor_id\": 0, \"temperature\": 23.04, \"pressure\": 1024.81, \"humidity\": 27.566, \"gas_resistance\": 119577.31353803856, \"air_quality\": 92.22874999999999, \"timestamp\": \"2021-01-15T14:39:18.001235\"}"

build: bin/redis-get

bin/redis-get: main/*.go lib/*.go
	cd main; go build -o ../bin/redis-get

.PHONY: clean
clean: redis-stop
	rm -f bin/redis-get

.PHONY: redis-start
redis-start:
	docker run -p $(REDIS_PORT):$(REDIS_PORT) --network $(DOCKER_NETWORK) --name $(REDIS_NAME) -d $(REDIS_TAG) \
		&& redis-cli set $(REDIS_KEY) $(REDIS_DATA)			

.PHONY: redis-stop
redis-stop: $(docker inspect $(REDIS_NAME) && [[ $? -eq 0 ]])
	docker stop $(REDIS_NAME)
	docker rm $(REDIS_NAME)


.PHONY: test
test: ./bin/redis-get
	./bin/redis-get localhost:$(REDIS_PORT) $(REDIS_KEY)