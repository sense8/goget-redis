# goget-redis

Get `key` from redis server.

```sh
./bin/redis-get <host:port> <key>
```

## build

```sh
make build
```

## test

```sh
make redis-start
make test
```

## clean

```sh
make clean
```
