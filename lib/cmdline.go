package cmdline

import (
	"context"

	redis "github.com/go-redis/redis/v8"
)

type RedisCtx struct {
	ctx context.Context
	cli *redis.Client
}

func Client(options *redis.Options) *RedisCtx {
	if options.Addr == "" {
		options.Addr = "localhost:6379"
	}
	rc := RedisCtx{}
	rc.ctx = context.Background()
	rc.cli = redis.NewClient(options)
	return &rc
}

func (ctx *RedisCtx) Get(key string) string {
	val, err := ctx.cli.Get(ctx.ctx, key).Result()
	switch {
	case err == redis.Nil:
		panic("KeyDoesNotExists")
	case err != nil:
		panic("GetFailed")
	}
	return val
}

func (ctx *RedisCtx) Set(key string, value string) error {
	return ctx.cli.Set(ctx.ctx, key, value, 0).Err()
}
