package main

import (
	"fmt"
	"os"

	redis "github.com/go-redis/redis/v8"
	cmdline "gitlab.com/sense8/goget-redis/lib"
)

func help() {
	fmt.Printf("[i] syntax:\n%s <host> <key>\n", os.Args[0])
}
func main() {
	if len(os.Args) < 3 {
		help()
		return
	}
	addr := os.Args[1]
	key := os.Args[2]
	if key == "" {
		panic("MissingKeyArgument")
	}
	ctx := cmdline.Client(&redis.Options{Addr: addr})
	fmt.Println(ctx.Get(key))
}
