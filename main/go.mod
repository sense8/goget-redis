module main

go 1.15

replace gitlab.com/sense8/goget-redis/lib => ../lib

require (
	github.com/go-redis/redis/v8 v8.4.8
	gitlab.com/sense8/goget-redis/lib v0.0.0-00010101000000-000000000000
)
